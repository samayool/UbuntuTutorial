1.  BETTER WAY : first install gdebi : > sudo apt -f install gdebi
					> sudo gdebi  <file.deb>

CLASSIC :
	.deb File : >dpkg -i /address/to/the/file.deb
		


	NOTE:You can install it using sudo dpkg -i /path/to/deb/file followed by sudo apt-get install -f .
	You can install it using apt-get install package_name . But first move your deb file to /var/cache/apt/archives/ directory.
	After executing this command, it will automatically download its dependencies.


2. Executable Files first make the file executable access : > chmod +x <filename> 
	then run the file : > ./<filename>

3. .bundle first make the file executable access : > chmod a+x <filename.bundle >
	then run the file : > ./<filename.bundle>

4. .tar.xz xvzf : UNCOMPRESS : > tar xvzf <filename.tar.xz>  or try this : > tar xf <filename.tar.xz>

