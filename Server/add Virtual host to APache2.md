> sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/socialboost.ir.conf


ADD TO THE FILE :


<VirtualHost *:80>
    ServerAdmin masood.m@gmail.com
    ServerName socialboost.ir
    ServerAlias www.socialboost.ir
    DocumentRoot /var/www/socialboost.ir/public_html
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>


OR 


<VirtualHost *:80>
    ServerAdmin masood.m@gmail.com
    ServerName persianbots.com
    ServerAlias www.persianbots.com
    DocumentRoot /var/www/persianbots.com/public_html
    ErrorLog ${APACHE_LOG_DIR}/persianbots_error.log
    CustomLog ${APACHE_LOG_DIR}/persianbots_access.log combined
</VirtualHost>



> sudo a2ensite socialboost.ir.conf
> sudo service apache2 restart

> sudo nano /etc/hosts

 ADD THIS LINE 

178.162.207.165	socialboost.ir         # <IP-address-server> TAB-CLICK Domain.com


FOLLOW "BIND DNS" in this LEARN folder    



#### SET NAME SERVERS OF DOMAIN :

ns1.persianbots.com
ns2.persianbots.com
